(function(R) {
    R.noti = {
        id : 'rb-noti',
        init: function() {
            if (!R.isset(document.getElementById(R.noti.id))) {
                var fragment = R.createElement('<div id="' + R.noti.id + '"></div>');
                document.body.insertBefore(fragment, document.body.firstChild);
            }
            R.noti.element = document.getElementById(R.noti.id);
            if (R.noti.element.hasChildNodes()) {
                R.noti.center();
                var divs = $('div.rb-success', '#' + R.noti.id);
                if (divs.length) {
                    setTimeout(function() {
                       divs.fadeOut(500, function() { $(this).remove(); })
                    }, 4000);
                }
            }

            $(document.body).on('click', '#' + R.noti.id + ' div', function() {
                $(this).fadeOut(500, function() { $(this).remove(); });
            });
        },
        clear: function(clearAll) {
            clearAll = R.default(clearAll, true);
            if (clearAll) {
                $('div', '#' + R.noti.id).remove();
                return;
            }
            $('div.rb-loading', '#' + R.noti.id).remove();
        },
        render: function(message, type) {
            type = R.default(type, 'success');
            R.noti.clear(type !== 'loading');
            var fragment = R.createElement('<div class="rb-' + type + '">' + message + '</div>');
            R.noti.element.appendChild(fragment);
            R.noti.center();
            if (type === 'success') {
                var div = $('div:last', '#' + R.noti.id);
                setTimeout(function() {
                       div.fadeOut(500, function() { $(this).remove(); })
                    }, 4000);
            }
        },
        loading: function() {
            R.noti.render('<i class="fa fa-spinner fa-pulse fa-lg fa-fw"></i>', 'loading');
        },
        success: function(message) {
            R.noti.render(message, 'success');
        },
        error: function(message) {
            R.noti.render(message, 'error');
        },
        center: function() {
            if (R.noti.element.offsetWidth < $(window).width()) {
                R.noti.element.style.transform = 'translate(-50%, 0)';
            } else {
                R.noti.element.style.transform = 'none';
            }
        }
    };

    R.modal = {
        id : 'modal-rebond',
        visible : '',
        init: function() {
            var fragment;
            if (!R.isset(document.getElementById(R.modal.id))) {
                fragment = R.createElement(
                    '<div id="' + R.modal.id + '" class="modal">' +
                        '<a class="close" href="#">x</a>' +
                        '<h1 class="mt"></h1>' +
                        '<div class="mt"></div>' +
                    '</div>'
                );
                document.body.insertBefore(fragment, document.body.firstChild);
            }

            $('.modal').appendTo(document.body);
            fragment = R.createElement('<div id="rb-overlay"></div>');
            document.body.insertBefore(fragment, document.body.firstChild);

            $(document.body).on('click', '.modal-link', function(e) {
                e.preventDefault();
                var modal = $(this).data('modal');
                var title = $(this).data('title');
                var cssClass = $(this).data('class');
                R.modal.render(modal, title, cssClass);
            });

            $(document.body).on('click', '.modal .close', function(e) {
                e.preventDefault();
                R.modal.clear();
            });

            $(document.body).on('click', '#rb-overlay', function() {
                R.modal.clear();
            });
        },
        render: function(modalId, title, cssClass) {
            title = title || '';
            cssClass = cssClass || '';
            var $modal = $('#modal-' + modalId);
            var fragment;

            // wrap text
            if (!$modal.children('div.mt').length) {
                $modal.wrapInner('<div class="mt"></div>');
            }
            // add title
            if (!$modal.find('h1.mt').length) {
                fragment = R.createElement('<h1 class="mt">' + title + '</h1>');
                $modal.prepend(fragment);
            } else {
                if (!$modal.children('h1.mt').length) {
                    $modal.find('h1.mt').prependTo($modal);
                }
                if (title !== '') {
                    $modal.find('h1.mt').html(title);
                }
            }
            // add close
            if (!$modal.children('.close').length) {
                fragment = R.createElement('<a class="close" href="#">x</a>');
                $modal.prepend(fragment);
            }
            $('#rb-overlay').show();
            $modal.addClass(cssClass).fadeIn().scrollTop(0);
            R.modal.visible = '#modal-' + modalId;
        },
        create: function(html, title, cssClass) {
            cssClass = cssClass || '';
            R.modal.visible = '#' + R.modal.id;
            $('h1.mt', R.modal.visible).html(title);
            $('div.mt', R.modal.visible).html(html);
            $('#rb-overlay').show();
            $(R.modal.visible).addClass(cssClass).fadeIn().scrollTop(0);
        },
        clear: function() {
            if (R.modal.visible === '') {
                return;
            }
            $(R.modal.visible).fadeOut(400, function() {
                $(R.modal.visible).removeClass().addClass('modal');
                $('#rb-overlay').fadeOut();
                R.modal.visible = '';
            });
        }
    };

    R.url = {
        hashList: function(index, url) {
            url = R.url.parseUrl(url);
            if (!R.isset(index)) {
                return url.hashList;
            }
            if (R.isset(url.hashList[index])) {
                return url.hashList[index];
            }
            return null;
        },
        hashMap: function(param) {
            var url = R.url.parseUrl();
            if (!R.isset(param)) {
                return url.hashMap;
            }
            if (R.isset(url.hashMap[param])) {
                return url.hashMap[param];
            }
            return null;
        },
        pathList: function(index) {
            var url = R.url.parseUrl();
            if (!R.isset(index)) {
                return url.pathList;
            }
            if (R.isset(url.pathList[index])) {
                return url.pathList[index];
            }
            return null;
        },
        pathMap: function(param) {
            var url = R.url.parseUrl();
            if (!R.isset(param)) {
                return url.pathMap;
            }
            if (R.isset(url.pathMap[param])) {
                return url.pathMap[param];
            }
            return null;
        },
        queryMap: function(param) {
            var url = R.url.parseUrl();
            if (!R.isset(param)) {
                return url.queryMap;
            }
            if (R.isset(url.queryMap[param])) {
                return url.queryMap[param];
            }
            return null;
        },
        parseUrl: function(url) {
            url = url || window.location.href;
            var parser = document.createElement('a');
            parser.href = url;
            var split, parts, i;

            parts = parser.pathname.split('/');
            var pathMap = {};
            for(i = 1; i < parts.length; i += 2) {
                pathMap[parts[i]] = parts[i + 1];
            }
            var pathList = [];
            for(i = 1; i < parts.length; i++) {
                pathList.push(parts[i]);
            }

            var queryMap = {};
            parts = parser.search.replace(/^\?/, '').split('&');
            if (parts[0] !== '') {
                for(i = 0; i < parts.length; i++) {
                    split = parts[i].split('=');
                    queryMap[split[0]] = split[1];
                }
            }

            var hashMap = {};
            var hashList = [];
            parts = parser.hash.replace(/^#/, '').split('/');
            if (parts[0] !== '') {
                for(i = 0; i < parts.length; i += 2) {
                    hashMap[parts[i]] = parts[i + 1];
                }
                for(i = 0; i < parts.length; i++) {
                    hashList.push(parts[i]);
                }
            }

            return {
                protocol: parser.protocol,
                host: parser.host,
                hostname: parser.hostname,
                port: parser.port,
                pathname: parser.pathname,
                hash: parser.hash.substring(1),
                pathList: pathList,
                pathMap: pathMap,
                query: parser.search,
                queryMap: queryMap,
                hashList: hashList,
                hashMap: hashMap
            };
        }
    };

    R.editor = {
        password: function() {
            var $pw = $('.password-viewer');
            if ($pw.length > 0) {
                $pw.click(function(e) {
                    e.preventDefault();
                });
                $pw.hover(
                    function() {
                        var id = $(this).data('id');
                        $('#' + id).attr('type', 'text');
                    },
                    function() {
                        var id = $(this).data('id');
                        $('#' + id).attr('type', 'password');
                    }
                );
            }
        },
        datePicker: function() {
            var $dp = $('.date-picker');
            if ($dp.length > 0) {
                $dp.datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    constrainInput: true,
                    changeMonth: true,
                    changeYear: true
                });
            }
        },
        fileSelector: function() {
            $('.file-remove').click(function(e) {
                e.preventDefault();
                var field = $(this).data('field');
                $('#remove-' + field).val(1);
                $('img', $(this).parent()).addClass('transparent');
                $(this).hide();
                $('#file-reset-' + field).show();
            });

            $('.file-reset').click(function(e) {
                e.preventDefault();
                var field = $(this).data('field');
                $('#remove-' + field).val(0);
                $('img', $(this).parent()).removeClass('transparent');
                $(this).hide();
                $('#file-remove-' + field).show();
            });

            $('.input', '.input-file').change(function() {
                var field = $(this).data('field');
                $('#file-clear-' + field).show();
            });

            $('.file-clear').click(function(e) {
                e.preventDefault();
                var field = $(this).data('field');
                $('#' + field).val('');
                $(this).hide();
            });
        }
    };

    R.lang = function(text, params) {
        if (text === '') {
            return '';
        }
        if (typeof lang[text] === 'undefined') {
            return text + '~';
        }
        if (typeof params === 'undefined') {
            return lang[text];
        }

        var locale = lang[text];
        var count = params.length;
        for (var i = 0; i < count; i++) {
            locale = locale.replace('{' + i + '}', params[i]);
        }
        return locale;
    };

    R.activator = function(elementId) {
        var $activator = $('.rb-activator');
        if (!$activator.length) {
           return;
        }
        $activator.each(function() {
            var $$ = $(this);
            // either go through all activators, or only elementId (used after an ajax request)
            if (typeof elementId !== 'undefined' && $$.attr('id') !== elementId) {
                return;
            }
            var active = $$.data('active');
            if (active === '') {
                return;
            }
            var $children = $$.children();
            $children.removeClass('active');
            $children.each(function() {
                var $child = $(this);
                if ($child.data('menu') === active) {
                    if ($$[0].nodeName === 'SELECT') {
                        $child.prop('selected', true);
                    } else {
                        $child.addClass('active');
                    }
                }
            });
        });
    };

    R.hashSelect = function(elementId, url) {
        var $select = $('select.rb-select');
        if (!$select.length) {
            return;
        }
        $select.each(function() {
            var $$ = $(this);
            if (typeof elementId !== 'undefined' && $$.attr('id') !== elementId) {
                return;
            }
            var hashKey = $$.data('hash');
            var $children = $$.children();
            $children.each(function() {
                var $child = $(this);
                if (R.url.hashList(hashKey, $child.val()) === R.url.hashList(hashKey, url)) {
                    $child.prop('selected', true);
                    return false;
                }
            });

        })
    };

    R.createElement = function(html) {
        var frag = document.createDocumentFragment();
        var temp = document.createElement('div');
        temp.innerHTML = html;
        while (temp.firstChild) {
            frag.appendChild(temp.firstChild);
        }
        return frag;
    };

    R.random = function(num) {
        return Math.floor(Math.random() * num);
    };

    R.isset = function(object) {
        return (typeof object !== 'undefined' && object !== null);
    };

    R.default = function(v, d) {
        return (typeof v !== 'undefined') ? v : d;
    };

})(window.R = window.R || {});
